// import './App.css';
// import React, {useState, useEffect} from 'react';
// // import React from 'react';
// function App() {
//   const [ghostpost, setGhostpost] = useState([]);
//   const url = "http://127.0.0.1:8000/api/post/";
//   useEffect(() => {
//     fetch(url)
//     .then(res => res.json())
//     .then(data => setGhostpost(data));
//   }, []);
//   return (
//     <div>
//     <h1>GHOST POST!</h1>
    
//     {ghostpost.map((p) => (
      
//       <ul>
//       <li>{p.post_type}</li>
//       console.log(post_type)
//       <li>Post: {p.text}</li>
//       <li>UpVotes:  {p.likes}</li>
//       <li>DownVotes: {p.dislikes}</li>
//       <li>Created: {p.created_at}</li>
//       <li>VoteScore: {p.vote_score}</li>
        
//       </ul>
      
//     ))}
    
//     </div>
//   );
// }

// export default App;

// https://reactjs.org/docs/handling-events.html
// looked through my Q2 assessments

import React from 'react';
import './App.css';
// import doggie from './images/ghostdog.jpg';
// <img src={doggie} />
// Jalon helped me figure out typos. Jalon rocks!

class App extends React.Component {
  constructor(props) {
    super(props);
      this.state = {
        post: [],

      }
    }

    getBoast = (event) => {
      fetch('http://localhost:8000/api/post/boast/')
      .then(res => res.json())
      .then(res => this.setState({ post: res }))
    }

    getRoast = (event) => {
      fetch('http://localhost:8000/api/post/roast/')
      .then(res => res.json())
      .then(res => this.setState({ post: res }))
    }

    highestVote = (event) => {
      fetch('http://127.0.0.1:8000/api/post/highest_votes/')
      .then(res => res.json())
      .then(res => this.setState({ post: res }))
    }

    scoreVote = (event) => {
      fetch('http://127.0.0.1:8000/api/post/vote_score/')
      .then(res => res.json())
      .then(res => this.setState({ post: res }))
    }

    getPosts = (event) => {
      fetch('http://localhost:8000/api/post')
      .then(res => res.json())
      .then(res => this.setState({ post: res }))
    }

    handleLike = (id) => {
      const requestVotes = {
        method: 'POST'
      };
      let data = fetch(`http://localhost:8000/api/post/${id}/like/`, requestVotes)
      .then(res => res.json())
      .then(res => this.getPosts())
    console.log(data)
    }
  

    handleDislike = (id) => {
      const requestVotes = {
        method: 'POST'
      };
      fetch(`http://localhost:8000/api/post/${id}/dislike/`, requestVotes)
      .then(res => res.json())
      .then(res => this.getPosts())
    }

    handleDeletePosts = (id) => {
      const requestVotes = {
        method: 'DELETE'
      };
      fetch(`http://localhost:8000/api/post/${id}/delete_post/`, requestVotes)
      .then(res => res.json())
      .then(res => this.getPosts())
    }


    render() {
    return (
      <div className='App'>
      
        <h2>GHOST POSTS!</h2>
        <button onClick={this.getPosts}>All posts</button>
        <button onClick={this.getBoast}>Boasts</button>
        <button onClick={this.getRoast}>Roasts</button>
        <button onClick={this.highestVote}>Highest Votes</button>
        <button onClick={this.addPost}>Create Post</button>

        {this.state.post.map((item) => {
          return (
            <ul>
            <li>
            {(item.post_type ? 
              (<span>Boast</span>)
              : (<span>Roast</span>)
            )}<br />
          Post: {item.text}<br />
          Vote Score: {item.vote_score}<br />
          Created: {item.created_at}<br />
            <button onClick={() => this.handleLike(item.id)}>Like {item.likes}</button>
            <button onClick={() => this.handleDislike(item.id)}>DisLike {item.dislikes}</button>
            <button onClick={() => this.handleDeletePosts(item.secret_key)}>Delete Post</button>

            </li>
            </ul>
          )
        })}
        </div>
      
      )
    }
    }

export default App;
